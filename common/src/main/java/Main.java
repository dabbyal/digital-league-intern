import java.util.*;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {
        List<Character> chars = new ArrayList<Character>() {{
            add('a');
            add('b');
            add('c');
            add('d');
            add('e');
        }};
        List<Person> persons = UtilPerson.createPersons();
        Map<Person, Integer> occurrences = new HashMap<>();
        List<Person> occurrencesList = new ArrayList<>();
        List<Person> moreThanThree = new ArrayList<>();

        Person template = UtilPerson.createPerson("Ivan", "Ivanov", "Ivanovich", 18, "Male", new Date(2000, 12, 12));
        Person template2 = UtilPerson.createPerson("Ivan", "Ivanov", "Ivanovich", 18, "Male", new Date(2000, 10, 10));

        for (Person person : persons) {
            System.out.println(person.getLastName() + " " + person.getAge());
        }

        Stream<Person> str = persons.stream();
        System.out.println("\nStarts with {a, b, c, d, e} and age < 20:");
        str
                .filter(person -> chars.contains(Character.toLowerCase(person.getLastName().charAt(0))))
                .filter(person -> person.getAge() < 20)
                .forEach(System.out::println);

        System.out.println("\nTEMPLATE: " + template);
        System.out.println("TEMPLATE2: " + template2);

        if (UtilList.isInList(persons, template)) {
            System.out.println("template object 1 is in arraylist");
        } else {
            System.out.println("template object 1 isn't in list");
        }

        if (UtilList.isInList(persons, template2)) {
            System.out.println("template object 2 is in arraylist");
        } else {
            System.out.println("obj 2 isn't in list");
        }

        persons.add(persons.get(3));
        persons.add(persons.get(3));
        persons.add(template2);
        persons.add(template);
        persons.add(persons.get(0));
        persons.add(persons.get(2));

        for (Person p : persons) {
            Integer oldValue = occurrences.get(p);
            if (oldValue == null) {
                oldValue = 0;
            }
            occurrences.put(p, oldValue + 1);
        }

        for (Person p : persons) {
            if (occurrences.containsKey(p) && occurrences.get(p) > 3) {
                moreThanThree.add(p);
                continue;
            }
            if (occurrences.containsKey(p) && occurrences.get(p) > 1) {
                occurrencesList.add(p);
            }
        }

        occurrencesList.sort(Collections.reverseOrder());
        System.out.println("\nDuplicated persons (not more than 3): ");
        for (int i = 0; i < occurrencesList.size(); i++) {
            System.out.println( i+1 + ": " + occurrencesList.get(i));
        }

        moreThanThree.sort(Collections.reverseOrder());
        System.out.println("\nDuplicated persons (more than 3): ");
        for (int i = 0; i < moreThanThree.size(); i++) {
            System.out.println( i+1 + ": " + moreThanThree.get(i));
        }

    }
}
