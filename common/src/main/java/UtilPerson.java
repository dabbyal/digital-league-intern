import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class UtilPerson {
    public static List<Person> createPersons() {
        return new ArrayList<Person>() {{
            add(createPerson("Ivan", "Ivanov", "Ivanovich", 18, "Male", new Date(2000, 10, 10)));
            add(createPerson("Arkadiy", "Arkamov", "Ivanovich", 23, "Female", new Date(1997, 10, 10)));
            add(createPerson("Genadiy", "Bvanov", "Ivanovich", 16, "Male", new Date(1978, 10, 10)));
            add(createPerson("Stepan", "Semenov", "Ivanovich", 68, "Female", new Date(1956, 10, 10)));
            add(createPerson("Vladimir", "Dvanov", "Ivanovich", 18, "Male", new Date(2000, 10, 10)));
            add(createPerson("Ivan", "Ivanov", "Ivanovich", 18, "Male", new Date(2000, 10, 10)));
        }};
    }

    public static Person createPerson(String firstName, String lastName, String middleName, int age, String gender, Date dob) {
        return new Person(firstName, lastName, middleName, age, gender, dob);
    }
}
