import java.util.List;

public class UtilList {
    public static <E> boolean isInList(List<E> list, E template) {
        for (E element : list) {
            if (element.equals(template)) return true;
        }
        return false;
    }
}
